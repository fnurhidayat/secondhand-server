const express = require("express");
const router = express.Router();
const { main } = require("../controllers/");

/* GET home page. */
router.get("/", main.index);

module.exports = {
  router,
  onError: main.onError,
  onLost: main.onLost,
};
